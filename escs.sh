##############################################################################
# push django-onesignal staging branch.
cd ~/projects/django-onesignal-code
# clean up all .pyc files. 
find . -name "*.pyc" -exec rm -f {} \;
git status
git add *
git commit -a
git push -u origin staging
#############################################################################
# push django-onesignal on master.
cd ~/projects/django-onesignal-code
# clean up all .pyc files. 
find . -name "*.pyc" -exec rm -f {} \;

git status
git add *
git commit -a
git push -u origin master
##############################################################################
# merge staging into master.
git checkout master
git merge staging
git push -u origin master
git checkout staging
##############################################################################
# merge master into staging.
git checkout staging
git merge master
git push -u origin staging
git checkout staging
##############################################################################
# create staging branch.
git checkout -b staging
git push --set-upstream origin staging


